package datastructure;

import java.util.Arrays;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int _rows;
    private int _cols;
    private CellState _cells[];

    public CellGrid(int rows, int columns, CellState initialState) {
        _rows = rows;
        _cols = columns;
        _cells = new CellState[rows * columns];
        Arrays.fill(_cells,initialState);
		// TODO Auto-generated constructor stub
	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return _rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return _cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        AssertInBounds(row, column);
        _cells[column + row * numColumns()] = element;  
        
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        AssertInBounds(row, column);
        return _cells[column + row * numColumns()];
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        var copy = new CellGrid(numRows(), numColumns(), CellState.DEAD);
        copy._cells = _cells.clone();
        return copy;
    }
    private void AssertInBounds(int row, int column) {
        if (0 > row || row >= numRows() || 0 > column || column >= numColumns())
            throw new IndexOutOfBoundsException();
    }
    
}
