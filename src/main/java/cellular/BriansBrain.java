package cellular;

import datastructure.IGrid;
import datastructure.CellGrid;
import java.util.Random;
public class BriansBrain implements CellAutomaton{
    
    IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }
    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < currentGeneration.numRows(); row++) {
            for (int col = 0; row < currentGeneration.numColumns(); col++) {
                currentGeneration.set(row, col, CellState.random(random));
            }
        }
    }
    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }
    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }
    @Override
    public CellState getCellState(int row, int col) {
        return currentGeneration.get(row, col);
    }
    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
        for (var row = 0; row < nextGeneration.numRows(); row++)
        for (var col = 0; col < nextGeneration.numColumns(); col++)
        nextGeneration.set(row, col, getNextCell(row, col));
        currentGeneration = nextGeneration;
    }
    @Override
    public CellState getNextCell(int row, int col) {
        var state = getCellState(row, col);
        if (state == CellState.ALIVE) return CellState.DYING;
        if (state == CellState.DYING) return CellState.DEAD;
        var neighborCount = countNeighbors(row, col, CellState.ALIVE);
        return neighborCount == 2 ? CellState.ALIVE : CellState.DEAD;
    }
    private int countNeighbors(int row, int col, CellState state) {
        var count = 0;
        for (var r = Math.max(row - 1, 0); r <= Math.min(row + 1, numberOfRows() - 1); r++)
        {
            for (var c = Math.max(col - 1, 0); c <= Math.min(row + 1, numberOfRows() - 1); c++)
            {
                var isNotThis = r != row || c != col;
                var hasSameState = getCellState(r, c).equals(state);
                count += isNotThis && hasSameState ? 1 : 0; 
            }
        }
        return count;
    }
    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
}
